<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%@ page import="com.swen90007.mvc.model.*" %>

<%
	Product product = (Product) session.getAttribute("product");
%>

<a href="?">Show all</a>

<h2>Product</h2>

<table border="1">
	<tr><td style="font-weight: bold;">Name</td><td><%=product.getName()%></td></tr>
	<tr><td style="font-weight: bold;">Price</td><td><%=product.getPrice()%></td></tr>
</table>

</body>
</html>