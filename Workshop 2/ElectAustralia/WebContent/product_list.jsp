<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%@ page import="com.swen90007.mvc.model.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>

<%
	HashMap<Long, Product> products = (HashMap<Long, Product>) session.getAttribute("products");
%>

<h2>Products</h2>

<table border="1">
	<tr style="font-weight: bold;"><td>Product</td><td>Price</td></tr>
<%

if (products != null) {
	for (Product product : products.values()) {
	    %>
	    <tr><td><a href="?id=<%=product.getId()%>"><%=product.getName()%></a></td><td><%=product.getPrice()%></td></tr>
	    <%
	}	
} else {
	%><tr><td>There are no products.</td></tr><%
}

%>

</table>

</body>
</html>