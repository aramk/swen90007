package com.swen90007.mvc;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.swen90007.mvc.model.User;
import com.swen90007.mvc.service.LoginService;
import com.swen90007.mvc.service.ProductService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServletOpt2")
public class LoginServletOpt2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServletOpt2() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		LoginService loginService = new LoginService();
		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		boolean result = loginService.authenticate(userId, password);
		if (result) {
			response.sendRedirect("products");
			return;
		} else {
			response.sendRedirect("login.jsp");
			return;
		}
	}

}
