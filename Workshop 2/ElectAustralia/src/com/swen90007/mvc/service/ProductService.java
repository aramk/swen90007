package com.swen90007.mvc.service;

import java.util.HashMap;

import com.swen90007.mvc.model.Product;

public class ProductService {
	
	HashMap<Long, Product> products = new HashMap<Long, Product>();
	
	public ProductService() {
		addProduct(new Product("Apple", 10));
		addProduct(new Product("Banana", 20));
	}

	public HashMap<Long, Product> getProducts() {
		return products;
	}
	
	public void addProduct(Product product) {
		products.put(product.getId(), product);
	}
	
	public Product getProduct(long id) {
		return products.get(id);
	}

}
