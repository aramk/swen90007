package com.swen90007.mvc.model;

import java.io.Serializable;

public class Model implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	
	public Model() {
		id = getNextID();
	}
	
	private static long nextID = 0;
	
	public static long getNextID() {
		return nextID++;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
