package com.swen90007.mvc;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.swen90007.mvc.service.ProductService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/products")
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ProductService productService = new ProductService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// TODO better way?
		Long productId;
		try {
			productId = Long.valueOf(request.getParameter("id"));			
		} catch (Exception e) {
			productId = null;
		}
		
		if (productId == null) {
			request.getSession().setAttribute("products", productService.getProducts());
			RequestDispatcher dispatcher = request.getRequestDispatcher("product_list.jsp");
			dispatcher.forward(request, response);
			return;
		} else {
			request.getSession().setAttribute("product", productService.getProduct(productId));
			RequestDispatcher dispatcher =  request.getRequestDispatcher("product.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
	}

}
