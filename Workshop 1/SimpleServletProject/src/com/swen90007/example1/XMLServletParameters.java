package com.swen90007.example1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SimpleServlet
 */
@WebServlet("/xmlServletParameters")
public class XMLServletParameters extends XMLServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		System.out.println("Hello from GET method");
		PrintWriter writer = response.getWriter();
		String userName = request.getParameter("userName");
		String userId = request.getParameter("userId");
		writer.println("<h3> Hello from Get [" + userName + "] [" + userId
				+ "]</h3>");

		// E.g. access
		// http://localhost:8080/SimpleServletProject/XMLServletParameters?test=123&test=456

		Enumeration<String> paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();
			List<String> paramValues = Arrays.asList(request
					.getParameterValues(paramName));
			for (String value : paramValues) {
				System.out.println("name: " + paramName + ", " + value);
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter();
		String userName = request.getParameter("userName");
		String fullName = request.getParameter("fullName");
		String profession = request.getParameter("profession");
		String location = request.getParameter("location");
		writer.println("<h3>Hello from Post " + userName + ", Full Name "
				+ fullName + ". You are a " + profession + " at Location "
				+ location + ".</h3>");
		
	}

}
