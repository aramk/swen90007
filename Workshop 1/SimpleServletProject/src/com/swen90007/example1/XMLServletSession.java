package com.swen90007.example1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SimpleServlet
 */
@WebServlet("/xmlServletSession")
public class XMLServletSession extends XMLServletParameters {
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException {
		// Called once before the first access to doGet or doPost
		System.out.println("Init");
	}
	
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// Called on each request. Is passed the request and response variables like doGet and doPost.
		super.service(req, res);
		System.out.println("Service");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		System.out.println("Hello from GET method");
		PrintWriter writer = response.getWriter();
		String userName = request.getParameter("userName");
		String userId = request.getParameter("userId");

		HttpSession session = request.getSession();
		
		boolean valid = userName != null && !userName.equals("");
		
		if (valid) {
		    session.setAttribute("savedUserName", userName);
		} else {
			userName = (String) session.getAttribute("savedUserName");			
		}
		
		writer.println("<h3> Hello from Get [" + userName + "] [" + userId
				+ "]</h3>");
		
		// E.g. access
		// http://localhost:8080/SimpleServletProject/XMLServletParameters?test=123&test=456

		Enumeration<String> paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();
			List<String> paramValues = Arrays.asList(request
					.getParameterValues(paramName));
			for (String value : paramValues) {
				System.out.println("name: " + paramName + ", " + value);
			}
		}

	}

}
