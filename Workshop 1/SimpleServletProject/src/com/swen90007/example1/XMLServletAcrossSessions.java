package com.swen90007.example1;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

/**
 * Servlet implementation class SimpleServlet
 */
@WebServlet("/xmlServletAcrossSessions")
public class XMLServletAcrossSessions extends XMLServletSession {
	private static final long serialVersionUID = 1L;

	// TODO these config reads don't work
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		System.out.println("defaultName: " + config.getInitParameter("defaultName"));
	}
	
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		super.service(req, res);
		System.out.println("defaultName: " + getServletConfig().getInitParameter("defaultName"));
	}
	
}
