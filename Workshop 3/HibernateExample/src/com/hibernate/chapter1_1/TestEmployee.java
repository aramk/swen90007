package com.hibernate.chapter1_1;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class TestEmployee {
	
	private static final Logger logger = Logger.getLogger(TestEmployee.class);

	public static void main(String[] args) {
		
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(Employee.class);

		// Read the default configuration file hibernate.configuration.xml
		// If the name of the configuration file name is different it can be
		// specified in the constructor.
		config.configure();

		// Create database table - read the method description to understand the
		// parameters
		new SchemaExport(config).create(true, true);

		// Create a session factory and get a session from the factory
		SessionFactory factory = config.buildSessionFactory();
		Session session = factory.getCurrentSession();

		// Create an Employee
		session.beginTransaction();
		Employee alex = new Employee();
		alex.setId(100);
		alex.setName("Alice Green");

		// Save the data and commit
		session.save(alex);
		session.getTransaction().commit();
		
		// Read the data back
		session = factory.openSession();
		session.beginTransaction();
		Employee alexDb = (Employee) session.load(Employee.class, alex.getId());
		logger.info(alexDb);
		session.getTransaction().commit();

	}

}
